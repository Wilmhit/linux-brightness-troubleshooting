#!/bin/sh

# Change the line below according to your hardware
BRIGHTNESS_FILE="/sys/class/backlight/amdgpu_bl0/brightness"
SAVE_TO="/usr/share/brightness/last_brightness"
BRIGHTNESS=$(cat "$BRIGHTNESS_FILE")
echo $BRIGHTNESS > "$SAVE_TO"
printf "Saved brightness: %i" $BRIGHTNESS

