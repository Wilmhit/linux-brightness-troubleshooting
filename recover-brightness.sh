#!/bin/sh

BRIGHTNESS_FILE="/var/lib/systemd/backlight/pci-0000:03:00.0:backlight:amdgpu_bl0"
SAVED_BRIGHTNESS="/usr/share/brightness/last_brightness"

if [[ -f $SAVED_BRIGHTNESS ]] ; then
    BRIGHTNESS=$(cat "$SAVED_BRIGHTNESS")
    echo $BRIGHTNESS > "$BRIGHTNESS_FILE"
fi

