# Linux brightness troubleshooting

If you have trouble with brightness save/restore functionality
this guide is probably for you.

## Requirements

 - I use Manjaro with KDE. DE does not really matter when dealing with
 systemd-backlight, but distribution does. Your files might be in
 different locations than mine.

 - You use systemd-backlight to save/restore brightness. You can check
 that by running `systemctl status systemd-backlight@<CLICK TAB>`. If
 there is anything resembling following then you are using it.

 ```
 ● systemd-backlight@backlight:amdgpu_bl0.service - Load/Save Screen Backlight Brightness of backlight:amdgpu_bl0
     Loaded: loaded (/usr/lib/systemd/system/systemd-backlight@.service; static)
     Active: inactive (dead)
       Docs: man:systemd-backlight@.service(8)
 ```

## What do you need to know

 - Where is your brightness file. Mine is in 
 `/sys/class/backlight/amdgpu_bl0/brightness`. On any Arch-based system
 this will be in `/sys/class/backlight/<SOMETHING>/brightness`. This
 file should contain a value from 0 to 255. If it contains higher value
 then your amdgpu driver is bugged. Nvidia users should not have
 problems like that. I will refer to this as *current brightness file*.

 - Where systemd-backlight saves your brightness. In my case this is
`/var/lib/systemd/backlight/pci-0000:03:00.0:backlight:amdgpu_bl0`.
Look for something like `/var/lib/systemd/backlight/<SOMETHING>`. I
will refer to it as *systemd brightness file*. It should also contain
value in range 0-255

## How systemd-backlight works?

On shutdown it copies *current brightness file* to
*systemd brightness file*. On boot it copies contents the other way.

## Debugging

First `cat` *current brightness file*. Make sure it's something
different than *systemd brightness file*. Reboot and check if
*systemd brightness file* has been updated to what was in
*current brightness file* before reboot.

If that is working, but brightness after reboot is still not set to
this value then systemd-backlight cannot write to
*current brightness file* for some reason.

If value in *systemd brightness file* is not what you set in
*current brightness file* before reboot then provided script will fix
this.

## What it does

 - save-brightness.sh copies contents of *current brightness file* to
 temp location.
 - restore-brightness.sh copies contents of temp location to
 *systemd brightness file*.
 - save-brightness.timer launches save-brightness.service every minute.
 - save-brightness.service launches save-brightness.sh.
 - restore-brightenss.service launches restore-brightness.sh before
 systemd-backlight starts on boot.

## Installation

 1. Copy \*.timer and \*.service files to `/usr/lib/systemd/system/`.
 2. Copy \*.sh files to `/usr/share/brightness` folder.
 3. Change paths in \*.sh files to reflect *systemd brightness file*
 and *current brightness file*.
 4. `chmod +x recover-brighteness.sh save-brightness.sh` as root.
 5. `systemctl enable save-brightness.timer restore-brightness.service`
 6. Will start working from next boot.

